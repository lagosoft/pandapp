import React from 'react';
import './Footer.css';

const Footer = () => {
  return (
    <div>
      <footer className="footer is-info">
        <div className="container">
          <div className="columns">
            <div className="column">
              <p>Panda app is created by musicians that love technology and music!</p>
            </div>
            <div className="column has-text-right">
              <a className="icon" href="http://facebook.com"><i className="fa fa-facebook"></i></a>
              <a className="icon" href="http://twitter.com"><i className="fa fa-twitter"></i></a>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
};

export default Footer;
