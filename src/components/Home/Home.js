import React, { Component } from 'react';
import './Home.css';

class Home extends Component {
    render() {
        let prodName = "Pandap"
        let heading = "solutions for busy musicians & bands";
        let subheading = "Manage, organize, communicate and update on the go from your smartphone.";
        return (
            <div>
               <section className="hero">
                    <div className="hero-body">
                        <div className="container">
                        <h1 className="title">
                            <strong>{ prodName }</strong>
                            <span className="subtitle is-1">&nbsp;{ heading }</span>
                        </h1>
                        <div className="is-two-thirds column is-paddingless">
                            <h2 className="subtitle is-3">{ subheading }</h2>
                        </div>
                        <a href="/features" className="button is-large is-primary" id="learn">Learn More</a>
                        </div>
                    </div>
                </section> 

                <section className="section">
                    <div className="container">
                        <div className="columns pd is-desktop">
                        <div className="column is-1 has-text-centered">
                            <i className="fa fa-cog is-primary"></i>
                        </div>
                        <div className="column is-one-third-desktop">
                            <p className="title"><strong>We provide top logistics so that your band ... can succeed and blah blha blah</strong></p>
                        </div>
                        <div className="column">
                            <p className="title">Another testimonial 2017 : We provide top logistics so that your band ... can succeed and blah blha blah</p>
                        </div>
                        </div>
                    </div>

                    <div className="columns pd">
                        <div className="column">
                        <div className="card">
                            <div className="card-content">
                            <p className="title">For me, is the most inspirational as long as you treat it that way. Skylar Grey. Inspirational, Life, Failure. Happiness is a butterfly, which when pursued </p>
                            <p className="subtitle">- Patrik DelSmith</p>
                            </div>
                        </div>
                        </div>
                        <div className="column">
                        <div className="card">
                            <div className="card-content">
                            <p className="title">For me, is the my Life, Failure. Happiness is a butterfly, which when pursued </p>
                            <p className="subtitle">- Rob DelSmith</p>
                            </div>
                        </div>
                        </div>
                        <div className="column">
                        <div className="card">
                            <div className="card-content">
                            <p className="title">For me, is the most inspirad at the end of the day - as long as you treat it that way. Skylar Grey. Inspirational, Life, Failure. Happiness is a butterfly, which when pursued </p>
                            <p className="subtitle">- Erica DelSmith</p>
                            </div>
                        </div>
                        </div>
                    </div>
                    </section>
            </div>
        );
    }
}

export default Home;